﻿using AutoFixture.AutoMoq;
using AutoFixture;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using System.Collections.Generic;
using System;
using Xunit;
using FluentAssertions;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Linq;
using System.Globalization;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        public Partner CreateBasePartner()
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100
                    }
                }
            };

            return partner;
        }

        public SetPartnerPromoCodeLimitRequest CreaterequestPartnerPromoCodeLimitRequest()
        {
            var request = new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = DateTime.Now.AddDays(10),
                Limit = 100
            };
            return request;
        }

        //1. Если партнер не найден, то также нужно выдать ошибку 404;
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = null;
            SetPartnerPromoCodeLimitRequest newLimit = CreaterequestPartnerPromoCodeLimitRequest();


            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, newLimit);

            // Assert
            result.Result.Should().BeAssignableTo<NotFoundResult>();
        }

        //2. Если партнер заблокирован, то есть поле IsActive=false в классе Partner, то также нужно выдать ошибку 400;
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var partner = CreateBasePartner();
            partner.IsActive = false;
            SetPartnerPromoCodeLimitRequest newLimit = CreaterequestPartnerPromoCodeLimitRequest();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, newLimit);

            // Assert
            result.Result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        //3.Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал NumberIssuedPromoCodes, если лимит закончился, то количество не обнуляется;
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_NumberIssuedPromoCodesZero_ReturnsZero()
        {
            // Arrange
            var partner = CreateBasePartner();
            partner.NumberIssuedPromoCodes = 20;
            SetPartnerPromoCodeLimitRequest newLimit = CreaterequestPartnerPromoCodeLimitRequest();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, newLimit);

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var model = okResult.Value.As<PartnerResponse>();
            Assert.Equal(model.NumberIssuedPromoCodes, 0);
        }

        //4. При установке лимита нужно отключить предыдущий лимит;
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_OldLimitCansel_ReturnsBadRequest()
        {
            // Arrange
            var partner = CreateBasePartner();
            partner.NumberIssuedPromoCodes = 20;
            SetPartnerPromoCodeLimitRequest newLimit = CreaterequestPartnerPromoCodeLimitRequest();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, newLimit);

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            // т.е. у всех лимитов должен стоять CancelDate кроме одного у которого дата оканяания совпадает с новым который только что задали
            var model = okResult.Value.As<PartnerResponse>();
            var LimitNotCanseledAndNotNew = model.PartnerLimits.Where(l => l.CancelDate == null
                                                        && l.EndDate != newLimit.EndDate );
            Assert.Equal(LimitNotCanseledAndNotNew.Count(), 0);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitISNull_ReturnsBadRequest()
        {
            // Arrange
            var partner = CreateBasePartner();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, null);

            // Assert
            result.Result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        //5. Лимит должен быть больше 0;
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitZero_ReturnsBadRequest()
        {
            // Arrange
            var partner = CreateBasePartner();
            SetPartnerPromoCodeLimitRequest newLimit = CreaterequestPartnerPromoCodeLimitRequest();
            newLimit.Limit = 0;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, newLimit);

            // Assert
            result.Result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        //6.Нужно убедиться, что сохранили новый лимит в базу данных
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitSavedToBD_isVerify()
        {
            // Arrange
            var partner = CreateBasePartner();
            SetPartnerPromoCodeLimitRequest newLimit = CreaterequestPartnerPromoCodeLimitRequest();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            _partnersRepositoryMock.Setup(repo => repo.UpdateAsync(partner));

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, newLimit);


            // Assert
            //Результат Ok
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            //в response у партнера установлен новый лимит
            var model = okResult.Value.As<PartnerResponse>();
            var LimitNotCanseledAndNotNew = model.PartnerLimits.Where(l => l.CancelDate == null
                                                        && l.EndDate == newLimit.EndDate);
            Assert.Equal(LimitNotCanseledAndNotNew.Count(), 1);
            //убедимся что метод на изменение партнера в БД вызывался.
            _partnersRepositoryMock.Verify(p => p.UpdateAsync(partner));
        }

    }
}
