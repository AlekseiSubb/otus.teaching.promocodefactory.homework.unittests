﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PartnerPromoCodeLimitResponse
    {
        public Guid Id { get; set; }

        public Guid PartnerId { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime? CancelDate { get; set; }

        public DateTime EndDate { get; set; }

        public int Limit { get; set; }
    }
}